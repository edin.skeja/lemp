FROM ubuntu:trusty
MAINTAINER Edin Skeja <edin.skeja@river.se>
# Keep upstart from complaining
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

RUN  apt-get update && apt-get install -y software-properties-common && add-apt-repository ppa:nginx/development -y && add-apt-repository ppa:ondrej/php -y  && rm -rf /var/lib/apt/lists/*

#Adding user for running services
RUN useradd -m river

# install the software we need
RUN apt-get update && apt-get upgrade -y --force-yes -o Dpkg::Options::="--force-confold" \
  && apt-get install -y --force-yes curl sendmail libpng12-dev libjpeg-dev wget \
  unzip nginx php7.0 php7.0-fpm php7.0-curl php7.0-apc php7.0-intl python-setuptools \
  php7.0-cli php7.0-gd php7.0-mysql php7.0-mbstring php7.0-oauth php7.0-memcache php7.0-mcrypt supervisor \
  php7.0-imagick php7.0-tidy mysql-client git curl memcached mysql-server dnsmasq imagemagick gettext && rm -rf /var/lib/apt/lists/*

# mysql config
RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# MYSQL 5.7
RUN wget http://dev.mysql.com/get/mysql-apt-config_0.8.1-1_all.deb
RUN dpkg -i mysql-apt-config_0.8.1-1_all.deb

RUN apt-get update && apt-get install -y --force-yes mysql-server

#RUN mysql_upgrade

# nginx config
RUN sed -i -e"s/keepalive_timeout\s*65/keepalive_timeout 2/" /etc/nginx/nginx.conf
RUN sed -i -e"s/keepalive_timeout 2/keepalive_timeout 2;\n\tclient_max_body_size 100m/" /etc/nginx/nginx.conf
RUN sed -i -e"s/user www-data/user river/" /etc/nginx/nginx.conf
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# php-fpm config
# RUN service php7.0-fpm start
RUN sed -i -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/fpm/php.ini
RUN sed -i -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" /etc/php/7.0/fpm/php.ini
RUN sed -i -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" /etc/php/7.0/fpm/php.ini
RUN sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.0/fpm/php-fpm.conf
RUN sed -i -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i -e "s/user\s*=\s*www-data/user = river/g" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i -e "s/group\s*=\s*www-data/group = staff/g" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i -e "s/listen.owner\s*=\s*www-data/listen.owner = river/g" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i -e "s/;php_admin_value\[error_log\]/php_admin_value\[error_log\]/g" /etc/php/7.0/fpm/pool.d/www.conf
RUN find /etc/php/7.0/cli/conf.d/ -name "*.ini" -exec sed -i -re 's/^(\s*)#(.*)/\1;\2/g' {} \;
RUN echo "extension=mcrypt.so" > /etc/php/7.0/fpm/conf.d/20-mcrypt.ini


## php Display errors.
RUN sed -i -e 's/display_errors = Off/display_errors = On/' /etc/php/7.0/fpm/php.ini

# nginx conf
ADD ./files/geoip.conf /etc/nginx/conf.d/geoip.conf
ADD ./files/geoip_fastcgi.conf /etc/nginx/snippets/geoip_fastcgi.conf
RUN echo "include snippets/geoip_fastcgi.conf;" >> /etc/nginx/fastcgi.conf

# Add geoip_city DB
ADD ./files/GeoLiteCity.dat /usr/share/GeoIP/GeoLiteCity.dat
RUN ln -s /usr/share/GeoIP/GeoLiteCity.dat /usr/share/GeoIP/GeoCity.dat

#Hostname fix for Sendmail
ADD ./files/sendmail.sh /tmp/sendmail.sh
RUN chmod +x /tmp/sendmail.sh

#Install PHPmyAdmin
ADD https://files.phpmyadmin.net/phpMyAdmin/4.5.5.1/phpMyAdmin-4.5.5.1-all-languages.tar.gz /tmp/
RUN mkdir /sites
RUN tar -xzf /tmp/phpMyAdmin-4.5.5.1-all-languages.tar.gz -C /sites
RUN mv /sites/phpMyAdmin-4.5.5.1-all-languages /sites/phpmyadmin
ADD ./files/phpmyadmin_config.php /sites/phpmyadmin/config.inc.php
RUN chmod 700 /sites/phpmyadmin/config.inc.php && chown river:river /sites/phpmyadmin/config.inc.php

#Install PimpMyLog
RUN git clone https://github.com/potsky/PimpMyLog.git /sites/PimpMyLog
RUN chown river:river /var/log/nginx
ADD ./files/pimpmylog_config.php /sites/PimpMyLog/config.user.php

# Supervisor Config
RUN /usr/bin/easy_install supervisor
RUN /usr/bin/easy_install supervisor-stdout
ADD ./files/supervisord.conf /etc/supervisord.conf

#DNSmasq
RUN echo "address=/docker/127.0.0.1" >> /etc/dnsmasq.conf
RUN echo "server=/lan/192.168.15.1" >> /etc/dnsmasq.conf
RUN echo "nameserver 8.8.8.8" > /var/run/dnsmasq/resolv.conf
RUN echo "user=root" >> /etc/dnsmasq.conf

# Add these late, so not much needs rebuilding.
ADD ./files/nginx-site.conf /etc/nginx/sites-available/default

VOLUME ["/var/lib/mysql"]

ADD ./files/run.sh /run.sh
RUN chmod 755 /run.sh

# Run startscript
CMD ["/run.sh"]
